# Second Normal Form

A table satisfies the second normal form if it already satisfies the first normal form,
and additionally all attributes are dependent on the whole primary key and not just a part of it<sup>[1]</sup>.
As an example of this, we'll have a look at a table of sales:

```
SELECT * FROM sales;
```{{execute}}

This table records how many items of a certain product were sold on a particular day,
as well as how that product is called and how much a single item costs.

Just from a first glance at this data, we intuitively see that there's a lot of redundancy:
the product name and price are listed repeatedly in every record for that product,
potentially leading to the anomalies that were described on the previous step.
Let's have a closer look and see where exactly the design of this table went wrong.

To uniquely identify records in this table, at least two attributes are required since
no single attribute is unique: there are multiple sales per day, as well as per product,
and the prices and numbers of sold items aren't unique either.
A reasonable choice in this case would be the combination of date and product ID,
though the name could be used just as well instead of the ID.

When looking at the data more closely, we additionally see that the name and price
_only_ depend on the product ID, and not on the date;
only the number of sold items depends on both the date and product ID;
this violates the second normal form.

To solve this, we'll create a new table for the product information:

```
CREATE TABLE products (
    id      INTEGER,
    name    VARCHAR(15),
    price   INTEGER,

    PRIMARY KEY (id)
);
```{{execute}}

Next, we'll take the product information and copy it to this new table.
We also use the `distinct` keyword to make sure that each entry is only copied once:

```
INSERT INTO products (
    SELECT DISTINCT product_id, name, price
    FROM sales
);
```{{execute}}

To finish off, we will remove the product information from the sales table,
and add a foreign key constraint that will link it to the product table:

```
ALTER TABLE sales
    DROP COLUMN name,
    DROP COLUMN price,
    ADD FOREIGN KEY (product_id)
        REFERENCES products(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE;
```{{execute}}

We have now eliminated the redundant data from our database,
while managing to retain all the information.
To demonstrate this, we'll use a `JOIN` statement to recreate
the same result that was stored in just one table at the beginning of this step: 

```
SELECT *
FROM sales s
INNER JOIN products p ON s.product_id = p.id; 
```{{execute}}
