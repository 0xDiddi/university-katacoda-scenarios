# First Normal Form

For the first normal form, we'll have a look at a table of user data.
The data has already been set up, and we can have a look at it with the following command.

```
SELECT * FROM users;
```{{execute}}

The issue with this data, which should be fairly intuitive when thinking about working with this data,
and the one that is addressed by the first normal form, is the atomicity of data, or lack thereof.

Data in a database table is called atomic when every column only contains a singular value<sup>[1]</sup>,
which is clearly not the case here, since the "name" column contains both the first and last name,
the "address" column contains the street, zip code and city,
and the hobbies column contains whole lists of values separated by commas.

To see why this is a potential problem, consider the following query,
where we want to see all the users that live in Dortmund:

```
SELECT * FROM users WHERE address LIKE '%Dortmund%';
```{{execute}}

If you look closely, you will notice that only two of those four people actually live in Dortmund,
the other two just happen to live on a street that shares this name.
And since we can't easily differentiate city and street, they all appear with this query.

Of course in this case a regular expression might do the trick, since all addresses at least follow the same pattern,
but running computationally rather intensive pattern matching on a large dataset would be very inefficient.
Especially when considering that an atomic column that is used often like this could be indexed,
while pattern matching expressions, expecially more complex ones, often don't benefit from indexing,
or require pre-computed index columns, which in this case would be identical to transforming the table to
the first normal form.
 
The "hobbies" column on the other hand seems - apart from the performance considerations mentioned above - easy enough
to search through, but presents problems when it comes to updating its values.
Usually in a _relational_ database, managing _relationships_ is easy and and if the two entities whose relation
should be modified are known, can be done in a single operation.
Here however, the current value of the column must be read, then the modification must be done as a string operation
and only then can the new value be updated in the table.

The naive solution would be to split the single hobbies column into multiple columns, one for each value.
This however means that users could only ever have at most three hobbies in this case,
and searching would become more complex since now, three columns must be searched.
The right solution for this would be to create a new table for all hobbies,
and then link them back to the users through a user-hobby relation table that uses foreign keys.
