# Final words

In total, there are ten normal forms, six numbered ones and four named ones.
Describing each of these in adequate detail is beyond the scope of this katacoda,
as already the first three exceed the suggested limit by a fair margin.

An honorable mention though is the sixth and last normal form,
which is satisfied when the table has only the primary key and at most one other attribute<sup>[4]</sup>.
This effectively results in a column oriented database,
which is a concept that many modern DBMS offer for its benefits in analytical workloads.
Though in many of these cases, this representation is only used internally,
and towards the user, the table is presented as if it wasn't in sixth normal form.


## Sources

1. H. Hübscher, H.-J. Petersen, C. Rathgeber, K. Richter, and Dr. D. Scharf, „Software/Datenbanken“ in _IT-Handbuch (Tabellenbuch)_, 9. Auflage, Braunschweig, Niedersachsen: Bildungshaus Schulbuchverlage Westermann Schroedel Diesterweg Schöningh Winklers GmbH, 2015, p. 233
2. E.F. Codd, „Further Normalisation of the Data Base Relational Model“ in _Research Report / RJ / IBM / San Jose, California RJ909_, 1971, p. 34
3. A. Buckenhofer, „04 Modeling“ in Lecture @DHBW: Data Warehouse, Apr-2021
4. K. Downs and nvogel, „Would like to Understand 6NF with an Example“ on Stack Overflow, 29-Jan-2011. [Online].  
   Available: https://stackoverflow.com/questions/4824714/would-like-to-understand-6nf-with-an-example. [Accessed: 10-Apr-2021]. 