# Normalization and Anomalies

The objectives of normalizing a database beyond the first normalform have been formulated by
Codd in 1972 in his article "Further Normalization of the Data Base Relational Model":

1. To free the collection of relations from undesirable insertion, update and deletion dependencies.
2. To reduce the need for restructuring the collection of relations, as new types of data are introduced,
   and thus increase the life span of application programs.
3. To make the relational model more informative to users.
4. To make the collection of relations neutral to the query statistics,
   where these statistics are liable to change as time goes by.

-- E.F. Codd, "Further Normalisation of the Data Base Relational Model"<sup>[2]</sup>

## Anomalies

The most commonly taught aspect of these points are the anomalies, which will be explained briefly below.
For this, a table of users and their orders will be used, let's have a look:

```
SELECT * FROM orders;
```{{execute}}

### Update Anomaly

Let's look at the orders of the user with id 0:

```
SELECT * FROM orders WHERE user_id = 0;
```{{execute}}

Looking closely at the result, it appears that the address in one of the records has a typo,
stating the house number as 35 instead of 34, and another record seems to originate from when
the user was a year younger.

This issue where redundant records provide conflicting information is referred to as an update anomaly,
because it most often occurs when not all such duplicate entries are updated correctly.

### Insertion anomaly

An insertion anomaly describes the fact that we cannot insert data for a new user who hasn't yet ordered anything,
at least not without setting the order related columns to NULL and thus producing an incomplete record.

### Deletion anomaly

The deletion anomaly is similar to the insertion anomaly, and occurs since we cannot
(again unless we use NULL values and produce incomplete records) delete all orders associated with a user
without at the same time losing all information about that user.

## Relevance

To put into perspective why these principles that are quite old in the context of the modern technology landscape
are still relevant today, we'll have a look at one of the lecture slides<sup>[3]</sup>,
namely the one adressing the requirements for a modern OLTP application,
which should avoid redundancy whereever possible in order to ensure a maintainable and contradiction free model.

![Slide](./assets/oltp_requirements.png)
