# Third Normal Form

A table satisfies the third normal form if it already satisfies the second normal form,
and additionally there are no transitive dependencies,
meaning that all attributes exclusively dependent on the primary key and on no other non-key attribute<sup>[1]</sup>.

We'll look back at the orders table from the anomaly overview step for this one,
where we had the order ID as the primary key of the table.
Each order had the cost, shipping method, number of items, and the user as attributes.
For the user though, instead of holding just their ID, all attributes were saved.
This means we have a transitive dependency, where the order ID determines the user ID of the user who did the order,
upon which then all the other user attributes like their name and address were dependent.

To solve this, we follow a similar procedure like in the last step,
creating a new table for the users, copying the user data over,
and lastly modifying the orders table to use a foreign key instead of storing the user data directly.

Since we previously identified some inconsistent data in this table,
we also need to specify that we only want to use the ID to determine if two users are distinct.
In this case leaving the choice of what data is correct up to the DBMS is fine,
but in a real world setting, additional care should be taken to select the correct data.

```
CREATE TABLE users_3nf (
    id          INTEGER,
    first_name  VARCHAR(20),
    last_name   VARCHAR(20),
    age         INTEGER,
    street      VARCHAR(50),
    zip         INTEGER,
    city        varchar(20),

    PRIMARY KEY (id)
);

INSERT INTO users_3nf (
    SELECT DISTINCT ON (user_id)
        user_id, first_name, last_name, age, street, zip, city
    FROM orders
);

ALTER TABLE orders
    DROP COLUMN first_name,
    DROP COLUMN last_name,
    DROP COLUMN age,
    DROP COLUMN street,
    DROP COLUMN zip,
    DROP COLUMN city,

    ADD FOREIGN KEY (user_id)
        REFERENCES users_3nf(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE;
```{{execute}}

If you look closely, you will notice that there is actually another transitive dependency
in this data that we have not yet fixed: The name of the city depends on the zip code!
We should note that while in this dataset the dependency could go either way,
in practice this is not the case: consider a large city that has multiple zip codes that all
refer to the same city with the same name.
